package parte1;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ImageOperator
{
	
	public ImageOperator()
	{
		
	}
	
	public static BufferedImage imagePicker() throws IOException
	{
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new FileNameExtensionFilter("Imagenes JPG", "jpg"));
		int val = fc.showOpenDialog(null);
		if(val == fc.APPROVE_OPTION)
			return ImageIO.read(ImageIO.createImageInputStream(new FileInputStream(fc.getSelectedFile().getPath())));
		else
			return null;
	}
	
	public static void imageSaver(BufferedImage img) throws IOException
	{
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new FileNameExtensionFilter("Imagenes JPG", "jpg"));
		int val = fc.showSaveDialog(null);
		if(val == fc.APPROVE_OPTION)
		{
			ImageIO.write(img,"jpg",new File(fc.getSelectedFile().getPath() + ".jpg"));
			JOptionPane.showMessageDialog(null, "Se ha guardado con exito en: " + fc.getSelectedFile().getAbsolutePath() + " :D");
		}
	}
	
	public static ImageIcon iconMaker(BufferedImage imagen1, int w, int h)
	{
		return new ImageIcon(imagen1.getScaledInstance(w, h, Image.SCALE_SMOOTH));
	}
	
	public static int resultWidth(BufferedImage imagen1, BufferedImage imagen2)
	{
		if(imagen1.getWidth() <= imagen2.getWidth())
			return imagen1.getWidth();
		else
			return imagen2.getWidth();
	}
	
	public static int resultHeight(BufferedImage imagen1, BufferedImage imagen2)
	{
		if(imagen1.getHeight() <= imagen2.getHeight())
			return imagen1.getHeight();
		else
			return imagen2.getHeight();
	}
	
	public static int[][][] addition(int [][][] imagen1, int [][][] imagen2, int w, int h)
	{
		int imagen3[][][] = new int[w][h][3];
		for(int x = 0; x < w; x++)
		{
			for(int y = 0; y < h; y++)
			{
				imagen3[x][y][0] = (imagen1[x][y][0] + imagen2[x][y][0])/2;
				imagen3[x][y][1] = (imagen1[x][y][1] + imagen2[x][y][1])/2;
				imagen3[x][y][2] = (imagen1[x][y][2] + imagen2[x][y][2])/2;
			}
		}
		return imagen3;
	}
	
	public static int[][][] linearCombination(int [][][] imagen1, int [][][] imagen2, int w, int h, double alpha, double beta)
	{
		int imagen3[][][] = new int[w][h][3];
		for(int x = 0; x < w; x++)
		{
			for(int y = 0; y < h; y++)
			{
				imagen3[x][y][0] = (int)(alpha * imagen1[x][y][0] + beta * imagen2[x][y][0]);
				imagen3[x][y][1] = (int)(alpha * imagen1[x][y][1] + beta * imagen2[x][y][1]);
				imagen3[x][y][2] = (int)(alpha * imagen1[x][y][2] + beta * imagen2[x][y][2]);
			}
		}
		return imagen3;
	}
	
	public static int[][][] subtraction(int [][][] imagen1, int [][][] imagen2, int w, int h)
	{
		int imagen3[][][] = new int[w][h][3];
		for(int x = 0; x < w; x++)
		{
			for(int y = 0; y < h; y++)
			{
				imagen3[x][y][0] = 255/2 + (imagen1[x][y][0] - imagen2[x][y][0])/2;
				imagen3[x][y][1] = 255/2 + (imagen1[x][y][1] - imagen2[x][y][1])/2;
				imagen3[x][y][2] = 255/2 + (imagen1[x][y][2] - imagen2[x][y][2])/2;
			}
		}
		return imagen3;
	}
	
	public static int[][][] multiplication(int [][][] imagen1, int [][][] imagen2, int w, int h)
	{
		int imagen3[][][] = new int[w][h][3];
		for(int x = 0; x < w; x++)
		{
			for(int y = 0; y < h; y++)
			{
				imagen3[x][y][0] = (imagen1[x][y][0] * imagen2[x][y][0])/255;
				imagen3[x][y][1] = (imagen1[x][y][1] * imagen2[x][y][1])/255;
				imagen3[x][y][2] = (imagen1[x][y][2] * imagen2[x][y][2])/255;
			}
		}
		return imagen3;
	}
	
	public static BufferedImage imageProcessor(BufferedImage img1, BufferedImage img2, char op, double sPos)
	{
		BufferedImage result = new BufferedImage(ImageOperator.resultWidth(img1, img2), ImageOperator.resultHeight(img1, img2), BufferedImage.TYPE_INT_RGB);
		int imagen1[][][] = new int[ImageOperator.resultWidth(img1, img2)][ImageOperator.resultHeight(img1, img2)][3], imagen2[][][] = new int[ImageOperator.resultWidth(img1, img2)][ImageOperator.resultHeight(img1, img2)][3], imagen3[][][] = new int[ImageOperator.resultWidth(img1, img2)][ImageOperator.resultHeight(img1, img2)][3];
		for(int x = 0; x < ImageOperator.resultWidth(img1, img2); x++)
		{
			for(int y = 0; y < ImageOperator.resultHeight(img1, img2); y++)
			{
				Color c1 = new Color(img1.getRGB(x, y)); //
				imagen1[x][y][0] = c1.getRed();
				imagen1[x][y][1] = c1.getGreen();
				imagen1[x][y][2] = c1.getBlue();
				Color c2 = new Color(img2.getRGB(x, y));
				imagen2[x][y][0] = c2.getRed();
				imagen2[x][y][1] = c2.getGreen();
				imagen2[x][y][2] = c2.getBlue();
			}
		}
		switch(op)
		{
			case '+':
			{
				imagen3 = ImageOperator.addition(imagen1, imagen2, ImageOperator.resultWidth(img1, img2), ImageOperator.resultHeight(img1, img2));
				break;
			}
			case '-':
			{
				imagen3 = ImageOperator.subtraction(imagen1, imagen2, ImageOperator.resultWidth(img1, img2), ImageOperator.resultHeight(img1, img2));
				break;
			}
			case '#':
			{
				imagen3 = ImageOperator.linearCombination(imagen1, imagen2, ImageOperator.resultWidth(img1, img2), ImageOperator.resultHeight(img1, img2), (sPos), (1.0 - sPos));
				break;
			}
			case '*':
			{
				imagen3 = ImageOperator.multiplication(imagen1, imagen2, ImageOperator.resultWidth(img1, img2), ImageOperator.resultHeight(img1, img2));
				break;
			}
			default:
			{
				
			}
		}
		for(int x = 0; x < ImageOperator.resultWidth(img1, img2); x++)
		{
			for(int y = 0; y < ImageOperator.resultHeight(img1, img2); y++)
			{
				Color  c = new Color(imagen3[x][y][0], imagen3[x][y][1], imagen3[x][y][2]);
				result.setRGB(x, y, c.getRGB());
			}
		}
		return result;
	}
	
}
