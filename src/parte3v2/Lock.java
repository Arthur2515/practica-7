package parte3v2;

public interface Lock 
{
	public void requestCR(int pid);
	public void releaseCR(int pid);
}
